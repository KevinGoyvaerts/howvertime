import React from 'react';
import { ScrollView, View, Text, StyleSheet, Dimensions } from 'react-native';

export class Days extends React.Component {
  render() {
    return (
      <ScrollView horizontal={true} style={styles.daySlider}>
        <View style={styles.day} onPress="">
          {this.props.isToday === true ? (<Text style={styles.today}>Aujourd'hui</Text>) : (<Text></Text>)}
          <Text style={styles.dayName}>{this.props.dayName}</Text>
          <Text style={styles.dayNbr}>{this.props.day}/{this.props.month}</Text>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  daySlider : {
    width: 350,
    height:200
  },
  day:{
    backgroundColor:'#ffffff',
    padding:10,
    width:150,
    height:150,
    borderRadius:200/2,
    alignItems:'center',
    justifyContent:'center'
  },
  today:{
    fontFamily:'GothamRnd-Bold',
    color:'white',
    fontSize:18,
    marginBottom:25
  },
  dayName:{
    fontFamily:'GothamRnd-Light',
    color:'#000000',
    fontSize:20
  },
  dayNbr:{
    fontFamily:'GothamRnd-Medium',
    color:'#000000',
    fontSize:25
  }
})
