import React from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'
import { TabBar } from "./tabBar"

export default class ActiveTab extends React.Component{
  render () {
    const CurComp = this.props.router.getComponentForState( this.props.navigation.state )

    return (
      <View style={style.container}>
        <CurComp navigation={this.props.navigation} router={this.props.router} />
        <TabBar navigation={this.props.navigation} router={this.props.router} />
      </View>
    )
  }
}

const { width, height } = Dimensions.get('window')
const style = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:'#F9F9F9',
    height: height - 48
  }
})
