import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableWithoutFeedback, StatusBar } from 'react-native';
import { Font, Constants, Svg } from 'expo';
import { CurDay } from './CurDay'
import TimeSlider from './timeSlider'
import DayInfos from './DayInfos'

export default class DetailDay extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      fontLoaded : false
    };

    this.daysOfWeek = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    this.monthOfYear = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    this.today = new Date();

    /* on récupère tous les jours du mois pour pouvoir les afficher dans le cal */
    this.monthDays = new Date( this.today.getFullYear(), this.today.getMonth(), 0).getDate();

    /* check du premier jour */
    this.monthFirstDay = new Date(this.today.getFullYear(), this.today.getMonth(), 1);
  }

  async componentDidMount(){
    await Font.loadAsync({
      'GothamRnd-Medium' : require('./../fonts/GothamRnd-Medium.otf'),
      'GothamRnd-Light' : require('./../fonts/GothamRnd-Light.otf')
    });

    this.setState({fontLoaded : true});
  }

  showTimeslider(){
    this.props.navigation.navigate('timeslider');
  }

  render(){
    return (
      <View style={style.mainContainer}>
        <StatusBar backgroundColor="rgba(255, 255, 255, 0)" barStyle="light-content"/>
        {
          this.state.fontLoaded == true ? (
            <View>
              <CurDay fontLight={style.fontLight}/>

              <View style={style.dayDetailContainer}>
                <Text style={[style.dayDetailText, style.dayDetailMonth]}>{this.monthOfYear[this.today.getMonth()]}</Text>
                <Text style={[style.dayDetailText, style.dayDetailDay]}>{this.today.getDate()}</Text>
                <Text style={[style.dayDetailText, style.dayDetailDayname]}>{this.daysOfWeek[this.today.getDay()]}</Text>
              </View>

              <View style={style.infosContainer}>
                <TouchableWithoutFeedback onPress={() => this.showTimeslider()}>
                  <View style={style.infosButton}>
                    <Text style={style.infosTitles}>Début</Text>
                    <Text style={style.infosTimes}>00 : 00</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.showTimeslider()}>
                  <View style={[style.infosButton, style.infosMargin]}>
                    <Text style={style.infosTitles}>Fin</Text>
                    <Text style={style.infosTimes}>00 : 00</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.showTimeslider()}>
                  <View style={[style.infosButton, style.infosMargin]}>
                    <Text style={style.infosTitles}>Heure{"\n"}de travail</Text>
                    <Text style={style.infosTimes}>00 : 00</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.showTimeslider()}>
                  <View style={[style.infosButton, style.infosMargin]}>
                    <Text style={style.infosTitles}>Heure{"\n"}Supp</Text>
                    <Text style={style.infosTimes}>00 : 00</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
          ) :
          (
            <Text>LOL</Text>
          )
        }
      </View>
    )
  }
}

const {widthSize, heightSize} = Dimensions.get('window')
const style = StyleSheet.create({
  mainContainer : {
    flex:1,
    paddingTop:48,
    // width: widthSize,
    // height: heightSize-48,
    // backgroundColor:'#F9F9F9'
  },
  dayDetailContainer:{
    width: widthSize*12,
    backgroundColor:'white',
    marginTop: 20,
    paddingVertical: 10
  },
  dayDetailText: {
    textAlign: 'center',
    color: '#FFA113',
    fontFamily: 'GothamRnd-Medium'
  },
  dayDetailMonth: {
    fontSize: 30
  },
  dayDetailDay: {
    fontSize: 70
  },
  dayDetailDayname:{
    fontSize: 45,
    marginTop: -20
  },
  infosContainer: {
    width:'100%',
    paddingVertical:31
  },
  infosButton: {
    borderLeftWidth:5,
    borderLeftColor:'#FFA117',
    width:widthSize-5,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 50
  },
  infosTitles:{
    color:'#FFA113',
    fontFamily:'GothamRnd-Light',
    fontSize:20,
    width:'50%',
    textAlign:'left',
    alignItems:'stretch',
    justifyContent:'center',
  },
  infosTimes:{
    color:'#FFA113',
    fontFamily: 'GothamRnd-Medium',
    fontSize:30,
    width:'50%',
    textAlign:'right'
  },
  infosMargin:{
    marginTop:20
  },
  fontLight: {
    fontFamily:'GothamRnd-Light'
  },
  fontMedium: {
    fontFamily:'GothamRnd-Medium'
  },
})
