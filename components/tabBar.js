import React from 'react'
import { View, StyleSheet, Dimensions, TouchableWithoutFeedback, Animated, Easing } from 'react-native'
import { Svg } from 'expo'
import extractBrush from 'react-native-svg/lib/extract/extractBrush'

const Anim = {
  Path : Animated.createAnimatedComponent(Svg.Path),
  G : Animated.createAnimatedComponent(Svg.G),
  Circle : Animated.createAnimatedComponent(Svg.Circle),
};

export class TabBar extends React.Component{
  constructor(props){
    super(props);

    let sizes = Dimensions.get('window');
    let sizeTier = (sizes.width/3);
    let pos = (sizeTier/2) - 17.5;

    this.state = {
      circlePos : new Animated.Value(pos),
      circleScale : new Animated.Value(1),
      pos: pos,
      sizeTier: sizeTier,
      homeBG : new Animated.Value(1),
      monthsBG : new Animated.Value(0),
      settingsBG : new Animated.Value(0)
    };

    this.prevClickedB = 'home';

    this.state.homeBG.addListener(() => {
      var buttonBG = this.state.homeBG.interpolate({
        inputRange: [0, 1],
        outputRange: ['rgba(255, 161, 19, 1)', 'rgba(255, 255, 255, 1)']
      });
      this.homeB.setNativeProps({
        fill : extractBrush(buttonBG.__getValue())
      });
    });
    this.state.monthsBG.addListener(() => {
      var monthsBG = this.state.monthsBG.interpolate({
        inputRange : [0, 1],
        outputRange : ['rgba(255, 161, 19, 1)', 'rgba(255, 255, 255, 1)']
      });
      this.monthsBG.setNativeProps({
        fill : extractBrush(monthsBG.__getValue())
      });
    });

    this.state.settingsBG.addListener(() => {
      var settingsBG = this.state.settingsBG.interpolate({
        inputRange: [0, 1],
        outputRange: ['rgb(255, 161, 19)', 'rgb(255, 255, 255)']
      });
      var settingsBGReverse = this.state.settingsBG.interpolate({
        inputRange: [0, 1],
        outputRange: ['rgb(255, 255, 255)', 'rgb(255, 161, 19)']
      });
      this.settingsBG1.setNativeProps({
        fill : extractBrush(settingsBG.__getValue())
      });
      this.settingsBG2.setNativeProps({
        fill : extractBrush(settingsBG.__getValue())
      });
      this.settingsBG3.setNativeProps({
        stroke : extractBrush(settingsBG.__getValue()),
        fill : extractBrush(settingsBGReverse.__getValue())
      });
    });
  }

  changeScreen ( routeName ) {
    /* recupération de la position du ref */
    this.props.navigation.navigate(routeName);

    this.animateCircle(routeName);
  }

  animateCircle (routeName) {
    switch(routeName){
      case 'home':
        var pos = this.state.pos;
        break;
      case 'months':
        var pos = this.state.pos + this.state.sizeTier;
        break;
      case 'settings':
        var pos = this.state.pos + (this.state.sizeTier*2);
        break;
    }
    Animated.sequence([
      Animated.parallel([
        Animated.timing(
          this.state.circleScale,
          {
            toValue: 0.2,
            duration: 150
          }
        ),
        Animated.timing(
          this.state[this.prevClickedB+'BG'],
          {
            toValue: 0,
            duration:150
          }
        )
      ]),
      Animated.timing(
        this.state.circlePos,
        {
          toValue: pos,
          duration: 200,
          easing: Easing.circle
        }
      ),
      Animated.parallel([
        Animated.timing(
          this.state.circleScale,
          {
            toValue: 1,
            duration: 450,
            easing: Easing.elastic(1.5)
          }
        ),
        Animated.timing(
          this.state[routeName+'BG'],
          {
            toValue: 1,
            duration: 450,
            easing: Easing.elastic(1.5)
          }
        )
      ])
    ]).start();

    this.prevClickedB = routeName;
  }

  render () {
    return (
      <View style={style.barLineContainer}>
        <View style={style.topBar}></View>
        <View style={style.barContainer}>
          <Animated.View style={[style.buttonCircle, {left: this.state.circlePos, transform:[{scale:this.state.circleScale}]}]} ref='boutontest'></Animated.View>
          <TouchableWithoutFeedback onPress={() => this.changeScreen('home')}>
            <View style={style.button}>
                <Svg width={20} height={20} ref="home">
                  <Anim.Path ref={(ref) => this.homeB = ref} d="M16.1,6.7v-5c0-0.1-0.1-0.1-0.1-0.1h-2.2c-0.1,0-0.1,0.1-0.1,0.1v2.1c0,0.1-0.1,0.1-0.1,0.1 c0,0-0.1,0-0.1,0l-3.4-3.6c0-0.1-0.1-0.1-0.2,0c0,0,0,0,0,0L0.4,10.4c0,0.1,0,0.1,0,0.2c0,0,0.1,0,0.1,0h2c0.1,0,0.1,0.1,0.1,0.1
                    v5.9c0,0.1,0.1,0.1,0.1,0.1h5.9c0.1,0,0.1-0.1,0.1-0.1v-3.4c0-0.1,0.1-0.1,0.1-0.1h2.2c0.1,0,0.1,0.1,0.1,0.1v3.4
                    c0,0.1,0.1,0.1,0.1,0.1h5.9c0.1,0,0.1-0.1,0.1-0.1v-5.9c0-0.1,0.1-0.1,0.1-0.1h2c0.1,0,0.1-0.1,0.1-0.1c0,0,0-0.1,0-0.1L16.1,6.7
                    C16.1,6.7,16.1,6.7,16.1,6.7z" fill="#FFFFFF" />
                </Svg>
              </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => this.changeScreen('months')}>
            <View style={style.button}>
              <Svg width={17} height={20} ref="months">
                <Anim.G fill="#FFA113" ref={(ref) => this.monthsBG = ref}>
                  <Svg.Path d="M3.9,4c0,0.4,0.3,0.7,0.7,0.7l0,0C5,4.7,5.3,4.4,5.3,4V0.6c0-0.2-0.1-0.4-0.3-0.5
                   		C4.7-0.1,4.2-0.1,4,0.2C3.9,0.3,3.8,0.5,3.8,0.6L3.9,4z"/>
                  <Svg.Path d="M11.9,4c0,0.4-0.3,0.7-0.7,0.7l0,0c-0.4,0-0.7-0.3-0.7-0.7V0.6c0-0.2,0.1-0.4,0.3-0.5
                  			c0.3-0.2,0.8-0.2,1,0.1C11.9,0.3,12,0.5,12,0.6L11.9,4z"/>
                  <Svg.Path d="M7.9,3h1.8V1.8H6V3H7.9z"/>
                  <Svg.Path d="M15.8,14.8V3.7c0-1-0.8-1.9-1.9-1.9h-1.3V3h1.1c0.3,0,0.5,0.2,0.5,0.5v2.7H1.6V3.5
                  		C1.6,3.2,1.7,3,2.1,3h1.1V1.8H1.9C0.9,1.8,0,2.6,0,3.7v11.1c0,1,0.7,1.9,1.8,1.9h0.1h12.2C15,16.7,15.8,15.9,15.8,14.8L15.8,14.8z
                  		 M14.2,14.8c0,0.3-0.2,0.5-0.5,0.5H2.1c-0.3,0-0.5-0.2-0.5-0.5V7.7h12.6C14.2,7.7,14.2,14.8,14.2,14.8z"/>
                  <Svg.Circle cx={3.9} cy={9.4} r={1.1} />
                  <Svg.Circle cx={7.9} cy={9.4} r={1.1} />
                  <Svg.Circle cx={11.9} cy={9.4} r={1.1} />
                  <Svg.Circle cx={3.9} cy={13.5} r={1.1} />
                  <Svg.Circle cx={7.9} cy={13.5} r={1.1} />
                  <Svg.Circle cx={11.9} cy={13.5} r={1.1} />
                </Anim.G>
              </Svg>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => this.changeScreen('settings')}>
            <View style={style.button}>
              <Svg width={18} height={20} ref="settings">
                <Svg.Defs>
                  <Svg.G id="ligne">
                    <Anim.Path ref={ref => this.settingsBG1 = ref} d="M2.6,1.1h11.9c0.7,0,1.2,0.6,1.2,1.2l0,0c0,0.7-0.6,1.2-1.2,1.2H2.6c-0.7,0-1.2-0.6-1.2-1.2 l0,0C1.3,1.7,1.9,1.1,2.6,1.1z" fill="#FFA113"/>
                    <Anim.Circle ref={ref => this.settingsBG2 = ref} fill="#FFA113" cx={12} cy={2.4} r={1.6}/>
                    <Anim.Circle ref={ref => this.settingsBG3 = ref} fill="#FFFFFF" stroke="#FFA113" stroke-width={1} cx={12} cy={2.4} r={1.6}/>
                  </Svg.G>
                </Svg.Defs>
                <Svg.Use href="#ligne"/>
                <Svg.Use href="#ligne" y={8} rotate={180} origin="8.5, 2"/>
                <Svg.Use href="#ligne" y={15}/>
              </Svg>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }
}

const {wSize, hSize} = Dimensions.get('screen')
const style = StyleSheet.create({
  barLineContainer:{
    width:wSize,
    height:48,
    alignItems: 'center',
    marginTop: 50
  },
  barContainer:{
    width:'100%',
    height: 47,
    flex:1,
    flexDirection: 'row',
    alignItems:'center',
    justifyContent:'center'
  },
  topBar: {
    width:'95%',
    height:1,
    backgroundColor: '#FFA117'
  },
  bar : {
    width: '100%',
    height: 23,
    flex:1,
    flexDirection : 'row',
    alignItems: 'center'
  },
  button: {
    flexGrow:1,
    flexShrink: 0,
    flex:1,
    justifyContent:'center',
    alignItems: 'center',
    width: wSize/3,
    height: '100%'
  },
  buttonCircle:{
    height:35,
    width:35,
    borderRadius:20,
    backgroundColor:'#FFA113',
    justifyContent: 'center',
    alignItems: 'center',
    position:'absolute'
  }
})
