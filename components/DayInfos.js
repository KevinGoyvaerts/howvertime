import React from 'react'
import { Animated, Dimensions, PanResponder, StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { Font } from 'expo'

/* les tailles avant la classe pour pouvoir les utiliser */
const {width, height} = Dimensions.get('window')

export default class DayInfos extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      fontLoaded : false,
    };
  }

  // async componentDidMount(){
  //   await Font.loadAsync({
  //     'GothamRnd-Medium' : require('./../fonts/GothamRnd-Medium.otf'),
  //     'GothamRnd-Light' : require('./../fonts/GothamRnd-Light.otf')
  //   });
  //
  //   this.setState({fontLoaded : true});
  // }


  render () {
    return (
      <View style={styles.globalContainer}>
        {this.props.children}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  globalContainer : {
    flex:1,
    paddingTop: 20
  }
  // container : {
  //   flex : 1,
  //   width : width,
  //   flexDirection : 'column',
  //   justifyContent : 'space-between',
  //   alignItems : 'stretch'
  // },
  // sliderContainer : {
  //   flexDirection : 'row',
  //   justifyContent : 'center',
  //   alignItems : 'center',
  //   height : '70%',
  //   overflow : 'hidden'
  // },
  // colLeft : {
  //   marginRight : 15,
  //   alignSelf : 'flex-start',
  //   backgroundColor : 'blue'
  // },
  // colRight : {
  //   marginLeft : 15,
  //   alignSelf : 'flex-start'
  // },
  // columnText : {
  //   fontFamily : 'GothamRnd-Medium',
  //   color : '#FFA113',
  //   fontSize : 50,
  //   textAlign : 'center'
  // },
  // title : {
  //   color: '#FFA113',
  //   fontFamily : 'GothamRnd-Light',
  //   fontSize : 20,
  //   textAlign : 'center',
  //   marginTop : 40,
  //   marginBottom : 11,
  //   flex : 0
  // },
  // gradient : {
  //   position:'absolute',
  //   left: 0,
  //   right : 0,
  //   height : 100
  // },
  // gradientTop : {
  //   top : 0
  // },
  // gradientBottom : {
  //   bottom : 0
  // },
  // buttonsContainer : {
  //   flexDirection : 'row',
  //   justifyContent : 'center',
  //   alignItems : 'center',
  //   width : width,
  //   marginBottom : 24
  // },
  // buttons : {
  //   width : '48%',
  //   alignItems : 'center'
  // }
})
