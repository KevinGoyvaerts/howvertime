import React from 'react'
import { View, StyleSheet, Dimensions, Text } from 'react-native'
import { Font } from 'expo'

export default class Settings extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      fontLoaded : false
    };
  }

  async componentDidMount(){
    await Font.loadAsync({
      'GothamRnd-Medium' : require('./../fonts/GothamRnd-Medium.otf'),
      'GothamRnd-Light' : require('./../fonts/GothamRnd-Light.otf')
    });

    this.setState({fontLoaded : true});
  }

  render () {
    return (
      <View style={style.settingsContainer}>
        {
          this.state.fontLoaded == true ? (
            <View>
              <View style={style.setting}>
                <Text style={style.settingTitle}>
                  Temps de midi
                </Text>
                <View style={style.settingButton}>
                  <Text style={style.settingButtonTxt}>
                    00 : 00
                  </Text>
                </View>
              </View>
              <View style={style.setting}>
                <Text style={style.settingTitle}>
                  Heures à prester / semaine
                </Text>
                <View style={style.settingButton}>
                  <Text style={style.settingButtonTxt}>
                    00 : 00
                  </Text>
                </View>
              </View>
            </View>
          ) : (
            <Text>LOL</Text>
          )
        }
      </View>
    )
  }
}

const {wSize, hSize} = Dimensions.get('screen')
const style = StyleSheet.create({
  settingsContainer:{
    flex:1,
    width: wSize,
    height: hSize,
    marginTop: 20,
    backgroundColor: '#F9F9F9'
  },
  setting:{
    width: '100%',
    marginTop:54
  },
  settingTitle:{
    fontFamily:'GothamRnd-Light',
    fontSize: 24,
    color: '#FFA113',
    textAlign: 'left',
    paddingHorizontal: 12,
    marginBottom: 12
  },
  settingButton: {
    width:'100%',
    backgroundColor :'#ffffff',
    paddingVertical: 5
  },
  settingButtonTxt: {
    textAlign: 'center',
    color:'#FFA113',
    fontFamily: 'GothamRnd-Medium',
    fontSize: 25
  }
})
