import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

export class CurDay extends React.Component{
  constructor(props){
    super(props)

    this.today = new Date();
    this.days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
    this.curMonth = this.today.getMonth()+1 < 10 ? '0'+(this.today.getMonth() + 1) : this.today.getMonth() + 1;
  }

  render () {
    return (
      <View style={style.curDayContainer}>
        <Text style={[style.todayText, style.todayDayname, this.props.fontLight]}>{this.days[this.today.getDay()]}</Text>
        <Text style={[style.todayText, style.todayDayMonthNbr, this.props.fontLight]}>{this.today.getDate()+'/'+this.curMonth}</Text>
      </View>
    )
  }
}

const style = StyleSheet.create({
  curDayContainer: {
    width: '100%'
  },
  todayText:{
    color:'#FFA113',
    textAlign:'center'
  },
  todayDayname:{
    fontSize:20
  },
  todayDayMonthNbr:{
    fontSize:39
  },
})
