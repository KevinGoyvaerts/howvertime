import React from 'react'
import { View, StyleSheet, Dimensions, Text, StatusBar } from 'react-native'
import { Font } from 'expo'
import { CurDay } from './CurDay'

export default class MonthDays extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      fontLoaded : false
    }

    this.daysOfWeek = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    this.monthOfYear = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    this.today = new Date();

    var d = new Date(this.today.getFullYear(), this.today.getMonth(), 0)
    this.nbrOfDays = d.getDate()
    this.firstDayOfMonth = d.getDay()
    var days = 1;
    this.days = new Array()
    for(var i = 0; i < this.nbrOfDays; i++){
      if( i < this.firstDayOfMonth ){
        this.days.push('nope')
      }
      else{
        this.days.push(days)
        days++;
      }
    }
  }

  async componentDidMount(){
    await Font.loadAsync({
      'GothamRnd-Medium' : require('./../fonts/GothamRnd-Medium.otf'),
      'GothamRnd-Light' : require('./../fonts/GothamRnd-Light.otf')
    });

    this.setState({fontLoaded : true});
  }

  render () {
    return (
      <View style={style.monthDaysContainer}>
      {
        this.state.fontLoaded == true ? (
          <View>
            <StatusBar backgroundColor="rgba(255, 255, 255, 0)" barStyle="light-content"/>
            <CurDay fontLight={style.fontLight}/>

            <View style={style.monthContainer}>
              <Text style={style.monthName}>
                {this.monthOfYear[this.today.getMonth()]}
              </Text>
              <View style={style.dayRow}>
                <Text style={style.dayNameCell}>Lu</Text>
                <Text style={style.dayNameCell}>Ma</Text>
                <Text style={style.dayNameCell}>Me</Text>
                <Text style={style.dayNameCell}>Je</Text>
                <Text style={style.dayNameCell}>Ve</Text>
                <Text style={style.dayNameCell}>Sa</Text>
                <Text style={style.dayNameCell}>Di</Text>
              </View>
              <View style={style.daysContainer}>
                {
                  this.days.map((item, key) => (
                    item == 'nope' ? (
                      <Text key={key} style={style.dayCell}></Text>
                    ) : (
                      <Text key={key} style={key > 6 ? [style.dayCell, style.dayCellRowNot1] : style.dayCell}>{item}</Text>
                    )
                  ))
                }
              </View>
              <View style={style.iContainer}>
                <Text style={style.i}>i</Text>
              </View>
            </View>
            <View style={style.monthInfosContainer}>
              <Text style={[style.infoLeft, style.info]}>00h00</Text>
              <Text style={[style.infoRight, style.info]}>05/30</Text>
            </View>
          </View>
        ) : (
          <Text>LOL</Text>
        )
      }
      </View>
    )
  }
}

const {width, height} = Dimensions.get('window')
const style = StyleSheet.create({
  monthDaysContainer : {
    flex:1,
    paddingTop:48,
    width: width,
    height: height,
    backgroundColor:'#F9F9F9'
  },
  monthContainer:{
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: 'white',
    marginTop: 20
  },
  monthName: {
    color:'#FFA113',
    fontFamily : 'GothamRnd-Medium',
    fontSize: 40,
    textAlign: 'center'
  },
  dayRow : {
    width: '100%',
    flexDirection: 'row'
  },
  dayNameCell:{
    color:'#FFA113',
    fontFamily: 'GothamRnd-Light',
    fontSize: 15,
    width: ((width-40)/7),
    textAlign: 'center'
  },
  dayCell : {
    color:'#FFA113',
    fontFamily: 'GothamRnd-Medium',
    fontSize: 15,
    width: ((width-40) / 7),
    textAlign: 'center'
  },
  dayCellRowNot1: {
    marginTop: 16
  },
  daysContainer: {
    width:'100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems:'flex-start',
    flexWrap: 'wrap',
    marginTop: 36
  },
  iContainer:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'flex-end',
  },
  i:{
    fontFamily:'GothamRnd-Medium',
    fontSize:25,
    color:'#FFA113',
    marginRight:20,
    marginTop:20
  },
  monthInfosContainer: {
    width:'100%',
    flexDirection:'row',
    alignItems:'flex-start',
    marginTop:10
  },
  infoLeft: {
    marginLeft: 20
  },
  infoRight:{
    marginLeft: 'auto',
    marginRight: 20,
  },
  info: {
    fontFamily:'GothamRnd-Light',
    fontSize: 20,
    color:'#FFA113'
  },
  fontLight : {
    fontFamily: 'GothamRnd-Light'
  }
})
