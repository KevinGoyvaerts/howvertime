import React from 'react'
import { Animated, Dimensions, PanResponder, StyleSheet, View, ScrollView, Text, TouchableOpacity } from 'react-native'
import { Font, LinearGradient, Svg } from 'expo'

/* les tailles avant la classe pour pouvoir les utiliser */
const {width, height} = Dimensions.get('window')

export default class TimeSlider extends React.Component {
  constructor (props) {
    super(props)

    this.keyProp = 0;
    this.date = new Date();

    this.state = {
      fontLoaded : false,
      hourPos : new Animated.Value( this.date.getHours() * 50 ),
      minPos : new Animated.Value( this.date.getMinutes() * 50 )
    };

    this.animatedColLeft = {
      paddingVertical : this.getPaddingVal(),
      top : this.state.hourPos
    };

    this.animatedColRight = {
      paddingVertical : this.getPaddingVal(),
      top : this.state.minPos
    };
  }

  componentWillMount () {
    this.timeResponder = PanResponder.create({
      onStartShouldSetPanResponder : (event, gestureState) => {
        console.log('je touche');
        return true;
      },
      onStartShouldSetPanResponderCapture : (event, gestureState) => false,
      onMoveShouldSetPanResponder : (event, gestureState) => {
        console.log(gestureState.dy);
        return true;
      },
      onMoveShouldSetPanResponderCapture : (event, gestureState) => false,
      onPanResponderMove : (event, gestureState) => Animated.event([
        null,
        {
          dy : this.state.hourPos
        }
      ]),
      onPanResponderRelease : (event, gestureState) => {
        console.log('j ai laché');
      },
      onShouldBlockNativeResponder: (e, gestureState) => true
    })
  }

  async componentDidMount(){
    await Font.loadAsync({
      'GothamRnd-Medium' : require('./../fonts/GothamRnd-Medium.otf'),
      'GothamRnd-Light' : require('./../fonts/GothamRnd-Light.otf')
    });

    this.setState({fontLoaded : true});
  }

  renderHours () {
    let hours = [];

    for(var i = 0; i < 24; i++){
      if( i < 10  ){
        hours.push(<Text key={this.keyProp} style={styles.columnText}>{'0' + i}</Text>)
      }
      else{
        hours.push(<Text key={this.keyProp} style={styles.columnText}>{i}</Text>)
      }

      this.keyProp++;
    }

    return hours;
  }

 renderMins () {
   let minutes = [];

   for(var i = 0; i < 60; i++){
     if( i < 10  ){
       minutes.push(<Text key={this.keyProp} style={styles.columnText}>{'0' + i}</Text>)
     }
     else{
      minutes.push(<Text key={this.keyProp} style={styles.columnText}>{i}</Text>)
     }

     this.keyProp++;
   }

   return minutes;
 }

 getPaddingVal () {
   let percentHeight =  ( height - 20 ) * 0.7 ;
   return ( percentHeight / 2 ) - 25;
 }

  render () {
    return (
      <View style={styles.globalContainer}>
        {
          this.state.fontLoaded == true ? (
            <View style={styles.container}>
              <Text style={styles.title}>Séléctionnez une heure</Text>
              <View style={styles.sliderContainer} removeClippedSubviews={true}>
                <View {...this.timeResponder.panHandlers} style={[styles.colLeft, this.animatedColLeft]}>
                  {this.renderHours()}
                </View>
                <View style={styles.column}>
                  <Text style={styles.columnText}>:</Text>
                </View>
                <View style={[styles.colRight, this.animatedColRight]}>
                  {this.renderMins()}
                </View>
                <LinearGradient style={[styles.gradient, styles.gradientTop]} colors={['rgba(255, 255, 255, 1)', 'rgba(255, 255, 255, 0)']} start={[0.5, 0]} end={[0.5, 1]}/>
                <LinearGradient style={[styles.gradient, styles.gradientBottom]} colors={['rgba(255, 255, 255, 1)', 'rgba(255, 255, 255, 0)']} start={[0.5, 1]} end={[0.5, 0]}/>
              </View>
              <View style={styles.buttonsContainer}>
                <TouchableOpacity style={styles.buttons}>
                  <Svg width={28.3} height={28.3}>
                    <Svg.Path fill="#FFA113" d="M18.9,14.4l7.7-7.7c1.3-1.3,1.3-3.3,0-4.5c-1.3-1.3-3.3-1.3-4.5,0l-7.7,7.7L6.7,2.2c-1.2-1.3-3.3-1.3-4.5,0
                    	c-1.3,1.3-1.3,3.3,0,4.5l7.7,7.7l-7.7,7.7c-1.3,1.2-1.3,3.3,0,4.5c0.6,0.6,1.4,0.9,2.3,0.9s1.6-0.3,2.3-0.9l7.7-7.7l7.7,7.7
                    	c0.6,0.6,1.4,0.9,2.3,0.9s1.6-0.3,2.3-0.9c1.3-1.2,1.3-3.3,0-4.5L18.9,14.4z" />
                  </Svg>
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttons}>
                  <Svg width={28.3} height={28.3}>
                    <Svg.Path fill="#FFA113" d="M13.2,26.7c-0.9,0-1.8-0.4-2.4-1.2L1.4,13.8c-1.1-1.3-0.9-3.3,0.5-4.4c1.3-1.1,3.3-0.9,4.4,0.5l6.6,8.3l9.4-15
                    	c0.9-1.5,2.8-1.9,4.3-1c1.5,0.9,1.9,2.8,1,4.3L15.8,25.2c-0.5,0.9-1.5,1.4-2.5,1.5C13.3,26.6,13.2,26.7,13.2,26.7z" />
                  </Svg>
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <Text>LOL</Text>
          )
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  globalContainer : {
    flex:1,
    paddingTop: 20
  },
  container : {
    flex : 1,
    width : width,
    flexDirection : 'column',
    justifyContent : 'space-between',
    alignItems : 'stretch'
  },
  sliderContainer : {
    flexDirection : 'row',
    justifyContent : 'center',
    alignItems : 'center',
    height : '70%',
    overflow : 'hidden'
  },
  colLeft : {
    marginRight : 15,
    alignSelf : 'flex-start',
    backgroundColor : 'blue'
  },
  colRight : {
    marginLeft : 15,
    alignSelf : 'flex-start'
  },
  columnText : {
    fontFamily : 'GothamRnd-Medium',
    color : '#FFA113',
    fontSize : 50,
    textAlign : 'center'
  },
  title : {
    color: '#FFA113',
    fontFamily : 'GothamRnd-Light',
    fontSize : 20,
    textAlign : 'center',
    marginTop : 40,
    marginBottom : 11,
    flex : 0
  },
  gradient : {
    position:'absolute',
    left: 0,
    right : 0,
    height : 100
  },
  gradientTop : {
    top : 0
  },
  gradientBottom : {
    bottom : 0
  },
  buttonsContainer : {
    flexDirection : 'row',
    justifyContent : 'center',
    alignItems : 'center',
    width : width,
    marginBottom : 24
  },
  buttons : {
    width : '48%',
    alignItems : 'center'
  }
})
