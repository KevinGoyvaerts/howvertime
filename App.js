import React from 'react'
import { StyleSheet, Text, View, StatusBar, Dimensions } from 'react-native'
import  DetailDay  from "./components/DetailDay"
import  Settings  from "./components/Settings"
import  ActiveTab  from "./components/ActiveTab"
import  MonthDays  from "./components/MonthDays"
import  TimeSlider  from "./components/timeSlider"
import { createNavigator, createNavigationContainer, TabRouter, addNavigationHelpers, withNavigation, createStackNavigator } from 'react-navigation';

const TimeSliderNavigator = createStackNavigator({
  timeslider : {screen: TimeSlider},
  detailDay : {screen: DetailDay}
}, {
  initialRouteName: 'detailDay',
  mode: 'modal',
  headerMode: 'none'
});

// ici c'est la création du router, le premier truc a passer pour construire le custom navigator
// le premier c'est la vue, le 2e c'est le routeur le 3e c'est les options
const MyRouter = TabRouter({
  home: {screen: TimeSliderNavigator},
  months : {screen: MonthDays},
  settings: {screen: Settings}
},{
  initialRouteName: 'home'
})

// la construction du composant de nav
const MyNavigator = createNavigationContainer(
  createNavigator(ActiveTab, MyRouter)
)

export default class App extends React.Component {
  render() {
    return (
      <MyNavigator router={MyRouter} />
    )
  }
}
